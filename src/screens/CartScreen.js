import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import Carts from '../components/carts/Carts';
import Food from '../icons/Food';

import {useSelector, useDispatch} from 'react-redux';
import {clearCart} from '../redux/actions/cartAction';
import {createOder} from '../redux/actions/orderAction';

const CartScreen = () => {
  const {inCart, totalPrice} = useSelector(state => state.cart);
  const {data} = useSelector(state => state.auth);

  const dispatch = useDispatch();

  console.log(data.userId);

  const addOder = () => {
    if (inCart.length > 0) {
      dispatch(createOder(inCart, totalPrice));
      ToastAndroid.showWithGravity(
        'Thanh toán thành công',
        ToastAndroid.SHORT,
        ToastAndroid.TOP,
      );
      dispatch(clearCart());
    } else {
      ToastAndroid.showWithGravity(
        'Hãy chọn món ăn bạn yêu thích',
        ToastAndroid.SHORT,
        ToastAndroid.TOP,
      );
    }
  };

  return (
    <>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Đơn Hàng</Text>
      </View>
      {inCart.length > 0 ? (
        <FlatList
          ListHeaderComponent={<Header />}
          data={inCart}
          keyExtractor={item => item.id}
          renderItem={({item}) => <Carts item={item} />}
          ListFooterComponent={<Footer />}
        />
      ) : (
        <View style={styles.cartEmty}>
          <Food />
          <Text style={styles.text}>Hãy chọn món ăn bạn yêu thích</Text>
        </View>
      )}

      <View style={styles.footerContainer}>
        <Text style={styles.footerText}>Thanh Toán</Text>
        <TouchableOpacity style={styles.checkout} onPress={() => addOder()}>
          <Text style={styles.textOder}>Đặt hàng</Text>
          <Text style={styles.totalPrice}>{totalPrice}đ</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

const Header = () => {
  return <React.Fragment />;
};

const Footer = () => {
  return <View />;
};

export default CartScreen;

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: 'snow',
    borderBottomWidth: 0.5,
    paddingTop: 15,
  },
  headerText: {
    marginLeft: 18,
    paddingBottom: 15,
    fontSize: 24,
    fontWeight: '700',
  },
  footerContainer: {
    // marginTop: 5,
    height: 110,
    width: '100%',
    backgroundColor: '#B39AFD',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  footerText: {
    width: '100%',
    textAlign: 'center',
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 5,
  },
  checkout: {
    backgroundColor: '#008B00',
    margin: 16,
    padding: 8,
    borderRadius: 26,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 9,
  },
  cartEmty: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textOder: {
    fontSize: 19,
    color: 'white',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  totalPrice: {
    fontSize: 19,
    color: 'white',
    fontWeight: 'bold',
    marginRight: 10,
  },
  text: {
    fontSize: 20,
  },
});
