import React from 'react';
import {StyleSheet, KeyboardAvoidingView} from 'react-native';
import LoginForm from '../components/loginForm/LoginForm';
import {AppStyles} from '../AppStyles';

// User thuc hien mot action gui den dispatch
import {createUser} from '../redux/actions/authAction';
// dispathcer sẽ được kích hoạt và gửi đến reducer một action
import {useDispatch} from 'react-redux';

const SigupScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const onSubmit = data => {
    dispatch(createUser(data.email, data.password));
  };

  //const onSubmit = data => console.log(data);

  return (
    <KeyboardAvoidingView style={styles.container}>
      <LoginForm
        isNewUser={true}
        data={{email: '', password: '', confirmPassword: ''}}
        onSubmit={onSubmit}
        navigation={navigation}
      />
    </KeyboardAvoidingView>
  );
};

export default SigupScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: AppStyles.primaryColor,
  },
});
