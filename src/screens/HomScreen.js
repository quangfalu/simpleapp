import React, {useEffect} from 'react';
import {View, FlatList} from 'react-native';
import CategoriesCard from '../components/categories/categoriesCard';
import Carousel from '../components/categories/categoriesCarousel';

import {useSelector, useDispatch} from 'react-redux';
import {fetchCategories} from '../redux/actions/categoriesAction';

const HomeScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const {catgories} = useSelector(state => state.categories);

  console.log(catgories);

  useEffect(() => {
    let mounted = true;
    if (mounted) {
      dispatch(fetchCategories());
    }

    return () => {
      mounted = false;
    };
  }, [dispatch]);

  return (
    <View>
      <FlatList
        ListHeaderComponent={<Header />}
        data={catgories}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <CategoriesCard item={item} navigation={navigation} />
        )}
        ListFooterComponent={<Footer />}
      />
    </View>
  );
};

const Header = () => {
  return (
    <React.Fragment>
      <Carousel />
    </React.Fragment>
  );
};

const Footer = () => {
  return <View />;
};

export default HomeScreen;

// const styles = StyleSheet.create({
//   list: {
//     paddingVertical: 10,
//   },
//   headerTitle: {
//     marginHorizontal: 16,
//   },
//   horizontalList: {
//     marginVertical: 16,
//     paddingHorizontal: 8,
//   },
//   verticalItem: {
//     marginVertical: 8,
//     marginHorizontal: 16,
//   },
//   horizontalItem: {
//     width: 256,
//     marginHorizontal: 8,
//   },
// });
