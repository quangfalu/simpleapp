/* eslint-disable no-shadow */
/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {View, FlatList} from 'react-native';
import Products from '../components/products/Products';
// import {
//     TouchableHighlight,
//     TouchableOpacity,
// } from 'react-native-gesture-handler';
import {firebase} from '../config/firebase';

const ProductScreen = ({navigation, route}) => {
  const item = route.params.item.id;
  const [products, setProducts] = useState(null);
  const numColumns = 2;
  const fetchProducts = async () => {
    const list = [];
    try {
      await firebase
        .database()
        .ref('data')
        .child(`products/${item}`)
        .once('value')
        .then(snap => {
          snap.forEach(data => {
            const {thumbnail, title, price} = data.val();
            list.push({
              id: data.key,
              thumbnail,
              title,
              price,
            });
          });
        });
      setProducts(list);
    } catch (error) {
      console.log(error);
    }
  };

  console.log(products);

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <FlatList
      ListHeaderComponent={<Header navigation={navigation} />}
      data={products}
      numColumns={numColumns}
      keyExtractor={item => item.id}
      renderItem={({item}) => <Products item={item} />}
      ListFooterComponent={<Footer />}
    />
  );
};

const Header = ({navigation}) => {
  return (
    <View
    //style={{ backgroundColor: 'snow', paddingTop: 8, paddingBottom: 16 }}
    >
      {/* <TouchableOpacity
                style={{ marginLeft: 18, marginTop: 12 }}
                onPress={() => navigation.navigate('HomeTab')}
            >
                <View>
                    <Image
                        style={{ height: 20, width: 30 }}
                        source={require('../../assets/back2.png')} />
                </View>
            </TouchableOpacity> */}
    </View>
  );
};

const Footer = () => {
  return <View />;
};

export default ProductScreen;
