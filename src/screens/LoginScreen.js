import React from 'react';
import {StyleSheet, KeyboardAvoidingView} from 'react-native';
import LoginForm from '../components/loginForm/LoginForm';
import {AppStyles} from '../AppStyles';

import {useDispatch} from 'react-redux';
import {loginUser} from '../redux/actions/authAction';

const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const onSubmit = data => {
    dispatch(loginUser(data.email, data.password));
  };
  // const onSubmit = data => console.log(data);

  const resetPasswordModal = () => {};

  return (
    <KeyboardAvoidingView style={styles.container}>
      <LoginForm
        isNewUser={false}
        data={{email: '', password: '', confirmPassword: ''}}
        onSubmit={onSubmit}
        navigation={navigation}
        resetPasswordModal={resetPasswordModal}
      />
    </KeyboardAvoidingView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: AppStyles.primaryColor,
  },
});
