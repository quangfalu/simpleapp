import React, {useEffect, useState} from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
// import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

import HomeScreen from './HomScreen';
import ProfileScreen from './ProfileScreen';
import CartScreen from './CartScreen';
import ProductScreen from './ProductScreen';
import LoginScreen from './LoginScreen';
import SignUpScreen from './SignUpScreen';
import Loading from '../components/loading/Loading';
import {AppStyles} from '../AppStyles';

import {useSelector, useDispatch} from 'react-redux';
import {authUser} from '../redux/actions/authAction';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();
// const TabTop = createMaterialTopTabNavigator();

const Navigator = () => {
  const dispatch = useDispatch();
  //useSelector access vào store để lấy dữ liệu cần thiết cho component.
  const {loading, login} = useSelector(state => state.auth);

  const [pageLoading, setPageLoading] = useState(true);

  useEffect(() => {
    dispatch(authUser());
    setPageLoading(loading);
  }, [dispatch, loading]);

  const HomeTab = () => {
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused, color, size}) => {
            if (route.name === 'HomeStack') {
              return (
                <View>
                  <Text>Home</Text>
                </View>
              );
            } else if (route.name === 'CartStack') {
              return (
                <View>
                  <Text>Home</Text>
                </View>
              );
            } else if (route.name === 'ProfileStack') {
              return (
                <View>
                  <Text>Home</Text>
                </View>
              );
            }
          },
        })}
        initialRouteName="Mangager"
        activeColor="orange"
        inactiveColor="gray"
        // eslint-disable-next-line react-native/no-inline-styles
        barStyle={{backgroundColor: 'white'}}>
        <Tab.Screen
          name="HomeStack"
          component={HomeScreen}
          options={{title: 'Khám phá'}}
        />
        <Tab.Screen
          name="CartStack"
          component={CartScreen}
          options={{title: 'Đơn hàng'}}
        />
        <Tab.Screen
          name="ProfileStack"
          component={ProfileScreen}
          options={{title: 'Thông tin tài khoản'}}
        />
      </Tab.Navigator>
    );
  };

  return pageLoading ? (
    <Loading backgroundcolor={AppStyles.colorBackground} />
  ) : (
    <NavigationContainer>
      <Stack.Navigator
      //headerMode="none"
      >
        {!login ? (
          <>
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="SignUp"
              component={SignUpScreen}
              options={{headerShown: false}}
            />
          </>
        ) : (
          <>
            <Stack.Screen
              name="HomeTab"
              component={HomeTab}
              options={{headerShown: false}}
            />
            <Stack.Screen
              name="ProductStack"
              component={ProductScreen}
              options={{title: 'Sản Phẩm'}}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigator;
