import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, FlatList} from 'react-native';
import {AppStyles} from '../AppStyles';
import ChangePassword from '../components/changePassword/changePassword';
import Frofile from '../components/profile/profile';

import {useDispatch} from 'react-redux';
import {logoutUser, changePassword} from '../redux/actions/authAction';

const data = [
  {
    id: 1,
    fullName: 'Quang',
    thumbnail:
      'https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.parentmap.com%2Fimages%2Farticle%2F8882%2Fteen-girl-thumbs-up-istock.jpg&f=1&nofb=1',
    email: 'quang@gmail.com',
    phoneNumber: '0987654321',
  },
];

const ProfileScreen = () => {
  return (
    <>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Thông tin tài khoản</Text>
      </View>
      <FlatList
        ListHeaderComponent={<Header />}
        data={data}
        keyExtractor={item => item.id}
        renderItem={({item}) => <Frofile info={item} />}
        ListFooterComponent={<Footer />}
      />
    </>
  );
};

const Header = () => {
  return <React.Fragment />;
};

const Footer = () => {
  const [modal, setModal] = useState(false);
  const dispatch = useDispatch();
  const submitInput = (password, newPassword) => {
    dispatch(changePassword(password, newPassword));
  };

  return (
    <View>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => dispatch(logoutUser())}>
        <Text style={styles.text}>Logout</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => setModal(true)}>
        <Text style={styles.text}>Change PassWord</Text>
      </TouchableOpacity>
      <ChangePassword
        isModalVisible={modal}
        closeDialog={() => setModal(false)}
        submitInput={(password, newPassword) =>
          submitInput(password, newPassword)
        }
      />
    </View>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({
  buttonContainer: {
    width: AppStyles.buttonWidth,
    height: AppStyles.buttonHeight,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: AppStyles.secondaryColor,
    borderRadius: AppStyles.borderRadiusMain,
    marginTop: 30,
  },

  headerContainer: {
    backgroundColor: 'snow',
    borderBottomWidth: 0.5,
    paddingTop: 15,
  },
  headerText: {
    marginLeft: 18,
    paddingBottom: 15,
    fontSize: 24,
    fontWeight: '700',
  },
  text: {
    fontSize: 20,
  },
});
