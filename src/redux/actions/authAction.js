// import Toast from 'react-native-toast-message';
// import { Platform } from 'react-native';
import {firebase} from '../../config/firebase';
import {clearAsyncStorage, setAsyncStorage, keys} from '../../AsyncStorage';

import {
  AUTH_USER_CREATE_FAIL,
  AUTH_USER_CREATE_SUCCESS,
  AUTH_USER_CREATE_INIT,
  AUTH_USER_INIT,
  AUTH_USER_FAIL,
  AUTH_USER_SUCCESS,
  AUTH_USER_LOGIN_INIT,
  AUTH_USER_LOGIN_FAIL,
  AUTH_USER_LOGIN_SUCCESS,
  AUTH_LOGOUT_INIT,
  AUTH_LOGOUT_SUCCESS,
  AUTH_LOGOUT_FAIL,
  AUTH_CHANGE_PASSWORD_FAIL,
  AUTH_CHANGE_PASSWORD_INIT,
  AUTH_CHANGE_PASSWORD_SUCCESS,
} from './actionTypes';

export const authUser = () => {
  return async dispatch => {
    dispatch({type: AUTH_USER_INIT});
    try {
      await firebase.auth().onAuthStateChanged(user => {
        if (user) {
          return dispatch({type: AUTH_USER_SUCCESS, payload: user});
        } else {
          return dispatch({
            type: AUTH_USER_FAIL,
            payload: {
              type: 'auth/user-not-login',
              message: 'Please Log In',
            },
          });
        }
      });
    } catch (error) {
      dispatch({type: AUTH_USER_FAIL, payload: error});
    }
  };
};

export const loginUser = (email, password) => {
  return async dispatch => {
    dispatch({type: AUTH_USER_LOGIN_INIT});
    let userData;
    try {
      const {user} = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      userData = user;
      setAsyncStorage(keys.uid, JSON.stringify(userData));
      // getAsyncStorage(keys.uid)
      console.log('Login successfully');
    } catch (error) {
      console.log(error);
      return dispatch({type: AUTH_USER_LOGIN_FAIL, payload: error});
    }
    return dispatch({type: AUTH_USER_LOGIN_SUCCESS, payload: userData});
  };
};

export const createUser = (email, password) => {
  return async dispatch => {
    dispatch({type: AUTH_USER_CREATE_INIT});
    let userData;
    try {
      const {user} = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);
      userData = user;
    } catch (error) {
      console.log(error);
      return dispatch({type: AUTH_USER_CREATE_FAIL, payload: error});
    }

    return dispatch({type: AUTH_USER_CREATE_SUCCESS, payload: userData});
  };
};

export const changePassword = (password, newPassword) => {
  return async dispatch => {
    dispatch({type: AUTH_CHANGE_PASSWORD_INIT});
    let firebaseUser = await firebase.auth().currentUser;
    const credential = firebase.auth.EmailAuthProvider.credential(
      firebaseUser.email,
      password,
    );
    let user;
    try {
      let response = await firebaseUser.reauthenticateWithCredential(
        credential,
      );
      user = response.user;
    } catch (error) {
      console.log(error);
      return dispatch({type: AUTH_CHANGE_PASSWORD_FAIL, payload: error});
    }
    try {
      await firebaseUser.updatePassword(newPassword);
      console.log('Password changed successfully');
    } catch (error) {
      console.log(error);
      return dispatch({type: AUTH_CHANGE_PASSWORD_FAIL, payload: error});
    }
    logoutUser();
    return dispatch({type: AUTH_CHANGE_PASSWORD_SUCCESS, payload: user});
  };
};

export const logoutUser = () => {
  return async dispatch => {
    dispatch({type: AUTH_LOGOUT_INIT});
    try {
      await firebase
        .auth()
        .signOut()
        .then(() => {
          clearAsyncStorage();
        });
    } catch (error) {
      console.log(error);
      return dispatch({type: AUTH_LOGOUT_FAIL, payload: error});
    }
    console.log('Logout successfully');
    return dispatch({type: AUTH_LOGOUT_SUCCESS});
  };
};
