// import { firebase } from '../../config/firebase'
// import { clearAsyncStorage, getAsyncStorage, setAsyncStorage, keys } from '../../AsyncStorage'

import {
  ADD_PRODUCT_TO_CART,
  INCREASE_PRODUCT_COUNT,
  DECREASE_PRODUCT_COUNT,
  DELETE_PRODUCT_FROM_CART,
  CLEAR_CART,
} from './actionTypes';

//const user = firebase.auth().currentUser.uid;
// export const addToCart = (item, quantity = 1) => {

//     let userData;
//     return async (dispatch) => {
//         try {
//             const { cart } = await firebase.database().ref('cart').child(`${user}/products`)
//                 .push({ ...item, quantity })
//             console.log("Success")
//             userData = cart

//         } catch (error) {
//             console.log(error)
//         }
//         dispatch({ type: ADD_TO_CART, payload: userData })
//     }
// }

// export const fetchCart = () => {
//     return async (dispatch) => {
//         const list = []
//         try {
//             await firebase.database().ref('cart').child(`${user}/products`).once('value')
//                 .then(snap => {

//                     if (snap.val()) {
//                         let dupIndex = 0;
//                         Object.keys(snap.val()).forEach(data => {

//                             const duplicate = (item, index) => {
//                                 if (item.id === snap.val()[data].id) {
//                                     dupIndex = index;
//                                     return true;
//                                 }
//                             };
//                             if (list.find(duplicate)) {
//                                 list[dupIndex].quantity += 1
//                             } else {
//                                 list.push({
//                                     key: data.id,
//                                     id: snap.val()[data].id,
//                                     quantity: snap.val()[data].quantity,
//                                     title: snap.val()[data].title,
//                                     price: snap.val()[data].price,
//                                     thumbnail: snap.val()[data].thumbnail
//                                 })
//                             }
//                             dupIndex = 0
//                         });
//                     }

//                 })
//         } catch (error) {
//             console.log(error)
//         }
//         dispatch({ type: FETCH_CART, payload: list })
//     }

// }

export const addToCart = product => ({
  type: ADD_PRODUCT_TO_CART,
  product,
});
export const increaseProductCount = product => ({
  type: INCREASE_PRODUCT_COUNT,
  product,
});
export const decreaseProductCount = product => ({
  type: DECREASE_PRODUCT_COUNT,
  product,
});
export const deleteFromCart = product => ({
  type: DELETE_PRODUCT_FROM_CART,
  product,
});
export const clearCart = () => ({
  type: CLEAR_CART,
});
