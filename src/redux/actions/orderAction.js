import {CREATE_ODER_FAIL, CREATE_ODER_SUCCESS} from './actionTypes';
import {firebase} from '../../config/firebase';

export const createOder = (inCart, totalPrice) => {
  const uid = firebase.auth().currentUser.uid;
  return async dispatch => {
    // eslint-disable-next-line no-shadow
    let createOder;
    try {
      const {create} = firebase
        .database()
        .ref(`cart/${uid}`)
        .push({order: {inCart, totalPrice}});
      createOder = create;
    } catch (error) {
      dispatch({type: CREATE_ODER_FAIL, payload: error});
    }
    dispatch({type: CREATE_ODER_SUCCESS, payload: createOder});
  };
};
