import {firebase} from '../../config/firebase';
import {
  FETCH_CATEGORIES_INIT,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_FAIL,
} from './actionTypes';

export const fetchCategories = () => {
  return async dispatch => {
    dispatch({type: FETCH_CATEGORIES_INIT});
    const list = [];
    try {
      await firebase
        .database()
        .ref('data')
        .child('catgories')
        .once('value')
        .then(snap => {
          snap.forEach(data => {
            const {image, title} = data.val();
            list.push({
              id: data.key,
              image,
              title,
            });
          });
        });
    } catch (error) {
      dispatch({type: FETCH_CATEGORIES_FAIL, payload: error});
    }
    dispatch({type: FETCH_CATEGORIES_SUCCESS, payload: list});
  };
};
