import {
  FETCH_CATEGORIES_INIT,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_FAIL,
} from '../actions/actionTypes';

const initialState = {
  catgories: [],
  error: null,
};

export const categoriesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_CATEGORIES_INIT:
      return {
        ...state,
      };
    case FETCH_CATEGORIES_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    case FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        catgories: action.payload,
        //categories: [...action.payload.categories],
        error: null,
      };
    default:
      return state;
  }
};
