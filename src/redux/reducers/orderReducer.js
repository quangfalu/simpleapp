import {CREATE_ODER_FAIL, CREATE_ODER_SUCCESS} from '../actions/actionTypes';

const initialState = {
  inCart: [],
  totalPrice: 0,
  error: null,
};

export const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_ODER_SUCCESS:
      return {
        ...state,
        inCart: action.payload,
        totalPrice: action.payload,
        error: null,
      };
    case CREATE_ODER_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};
