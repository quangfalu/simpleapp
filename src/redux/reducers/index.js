import {combineReducers} from 'redux';
import {authReducer} from './authReducer';
import {categoriesReducer} from './categoriesReducer';
import {cartReducer} from './cartReducer';
import {orderReducer} from './orderReducer';

// Gop cac reducer thanh mot rootReducer
export const rootReducer = combineReducers({
  auth: authReducer,
  categories: categoriesReducer,
  cart: cartReducer,
  order: orderReducer,
});
