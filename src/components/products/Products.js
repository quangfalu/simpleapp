import React from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import {useDispatch} from 'react-redux';
import {addToCart} from '../../redux/actions/cartAction';

const Products = ({item, navigation}) => {
  const {thumbnail, title, price} = item;
  const dispactch = useDispatch();

  return (
    <View style={styles.container}>
      <Image style={styles.photo} source={{uri: thumbnail}} />
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.price}>{price}đ</Text>
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => dispactch(addToCart(item))}>
        <Text style={styles.addToCartText}>Thêm</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Products;

const {width, height} = Dimensions.get('window');
const SCREEN_WIDTH = width < height ? width : height;
const recipeNumColums = 2;
const RECIPE_ITEM_HEIGHT = 200;
const RECIPE_ITEM_MARGIN = 20;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    width:
      (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) /
      recipeNumColums,
    height: RECIPE_ITEM_HEIGHT + 75,
    borderRadius: 15,
    marginHorizontal: 10,
  },
  photo: {
    width:
      (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) /
      recipeNumColums,
    height: RECIPE_ITEM_HEIGHT - 40,
    borderRadius: 16,
    borderColor: 'black',
  },
  title: {
    flex: 1,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'black',
    marginTop: 10,
    marginRight: 5,
    marginLeft: 5,
  },
  price: {
    marginTop: 0,
    marginBottom: 0,
    fontSize: 18,
    fontWeight: '600',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  buttonContainer: {
    width: 178,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: 'green',
    borderWidth: 1,
    borderRadius: 25,
    marginTop: 8,
  },
  addToCartText: {
    fontSize: 16,
    fontWeight: '700',
    color: 'green',
  },
});
