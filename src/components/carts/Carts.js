import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {TouchableHighlight} from 'react-native-gesture-handler';

import {
  decreaseProductCount,
  increaseProductCount,
} from '../../redux/actions/cartAction';
import {useSelector, useDispatch} from 'react-redux';

const Carts = ({item, navigation}) => {
  const dispatch = useDispatch();
  const {inCart} = useSelector(state => state.cart);

  return (
    <View style={styles.container}>
      <View style={styles.productContainer}>
        <View style={styles.productInfo}>
          <Text style={styles.productTitle}>{item.title}</Text>
          <Text style={styles.productPrice}>{item.price}đ</Text>
        </View>

        <View>
          <View style={styles.imageContainer}>
            <Image source={{uri: item.thumbnail}} style={styles.productImage} />
          </View>

          <View style={styles.touchAdd}>
            <TouchableHighlight
              style={styles.addToCart}
              disabled={
                inCart.find(product => product.id === item.id).count === 1
                  ? true
                  : false
              }
              onPress={() => dispatch(decreaseProductCount(item))}>
              <Text style={styles.addToCartText}>-</Text>
            </TouchableHighlight>
            <Text style={styles.quantity}>{item.count}</Text>
            <TouchableHighlight
              style={styles.addToCart}
              onPress={() => dispatch(increaseProductCount(item))}>
              <Text style={styles.addToCartText}>+</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Carts;

const styles = StyleSheet.create({
  container: {
    height: 200,
    padding: 20,
    margin: 1,
    borderBottomWidth: 3,
    borderColor: 'white',
    paddingHorizontal: 10,
    marginHorizontal: 8,
    justifyContent: 'space-between',
  },
  productContainer: {
    flexDirection: 'row',
  },
  imageContainer: {
    paddingHorizontal: 10,
  },
  productImage: {
    width: 110,
    height: 110,
    resizeMode: 'cover',
    borderRadius: 20,
  },
  productInfo: {
    flex: 1,
    flexDirection: 'column',
  },
  productTitle: {
    fontSize: 22,
    fontWeight: '700',
  },
  productPrice: {
    fontSize: 18,
    color: 'black',
    fontWeight: '100',
    fontFamily: 'sans-serif',
  },
  touchAdd: {
    flexDirection: 'row',
    marginTop: 24,
    marginHorizontal: 20,
    justifyContent: 'space-between',
  },
  addToCart: {
    height: 27,
    width: 27,
    backgroundColor: 'white',
    borderRadius: 25,
    borderWidth: 2,
    borderColor: 'green',
  },
  addToCartText: {
    bottom: 3,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'green',
    alignContent: 'center',
    textAlign: 'center',
    justifyContent: 'center',
  },
  quantity: {
    padding: 0,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});
