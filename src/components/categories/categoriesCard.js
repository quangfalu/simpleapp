import React from 'react';
import {View, ImageBackground, Text, StyleSheet} from 'react-native';
import {TouchableHighlight} from 'react-native-gesture-handler';

const CategoriesCard = ({item, navigation}) => {
  return (
    <TouchableHighlight
      underlayColor="rgb(242, 242, 242)"
      onPress={() => navigation.navigate('ProductStack', {item})}>
      <View style={styles.categoriesItemContainer}>
        <ImageBackground
          style={styles.categoriesPhoto}
          source={{uri: item.image}}
        />
        <Text style={styles.categoriesName}>{item.title}</Text>
      </View>
    </TouchableHighlight>
  );
};

export default CategoriesCard;

const styles = StyleSheet.create({
  categoriesItemContainer: {
    flex: 1,
    margin: 15,
    justifyContent: 'center',
    alignItems: 'center',
    height: 210,
    borderColor: '#cccccc',
    borderWidth: 2,
    borderRadius: 12,
    backgroundColor: 'white',
  },
  categoriesPhoto: {
    width: '100%',
    height: 160,
    borderRadius: 10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    shadowColor: 'white',
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    elevation: 3,
  },
  categoriesName: {
    flex: 1,
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#333333',
    marginTop: 8,
  },
  categoriesInfo: {
    marginTop: 3,
    marginBottom: 5,
  },
});
