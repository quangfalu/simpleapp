import React from 'react';
import {StyleSheet, ImageBackground, FlatList, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const data = [
  {
    id: 1,
    image:
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fberita.teknologi.id%2Fuploads%2F2019%2F01%2Fgo-food-istimewa.jpg&f=1&nofb=1',
  },
  {
    id: 2,
    image:
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fbisniswisata.co.id%2Fwp-content%2Fuploads%2F2019%2F04%2FGoFood.jpg&f=1&nofb=1',
  },
  {
    id: 3,
    image:
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fthucthan.com%2Fmedia%2F2019%2F09%2Fcac-loai-ngu-coc%2Fcac-loai-hat-ngu-coc.jpg&f=1&nofb=1',
  },
  {
    id: 4,
    image:
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ffile.vforum.vn%2Fhinh%2F2016%2F08%2Fhinh-nen-dep-ve-rau-qua-6.jpg&f=1&nofb=1',
  },
];

const CardHeader = ({item, navigation}) => {
  return (
    <TouchableOpacity onPress={() => navigation.navigate('HomeStack')}>
      <View style={styles.container}>
        <ImageBackground
          style={(styles.containerImage, styles.image)}
          source={{uri: item.image}}
        />
      </View>
    </TouchableOpacity>
  );
};

const Carousel = ({navigation}) => {
  return (
    <>
      <FlatList
        contentContainerStyle={styles.horizontalList}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        data={data}
        keyExtractor={item => item.id}
        renderItem={({item}) => (
          <CardHeader item={item} navigation={navigation} />
        )}
      />
    </>
  );
};

export default Carousel;

const styles = StyleSheet.create({
  container: {
    borderRadius: 30,
  },
  containerImage: {
    height: 200,
  },
  image: {
    height: 160,
    width: 300,
    paddingVertical: 24,
    paddingHorizontal: 16,
    marginHorizontal: 8,
  },
  horizontalList: {
    marginVertical: 16,
    paddingHorizontal: 8,
  },
});
