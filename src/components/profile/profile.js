import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const Profile = ({info}) => {
  const {fullName, email, phoneNumber, thumbnail} = info;
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Image style={styles.avatar} source={{uri: thumbnail}} />
        <View style={styles.info}>
          <Text style={styles.fullName}>{fullName}</Text>
          <Text style={styles.text}>{email}</Text>
          <Text style={styles.text}>{phoneNumber}</Text>
        </View>
        <Text>Edit</Text>
      </View>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  content: {
    flexDirection: 'row',
    margin: 15,
    justifyContent: 'space-between',
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  info: {
    flex: 0.9,
    borderWidth: 1,
    // width:280
  },
  fullName: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  text: {
    fontSize: 16,
  },
});
