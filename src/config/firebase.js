import * as firebase from 'firebase';
import '@firebase/auth';
import '@firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBqJyBfHmu1YZAjr38qL70LBzENp9RLhkE',
  authDomain: 'logapp-21fa6.firebaseapp.com',
  databaseURL: 'https://logapp-21fa6-default-rtdb.firebaseio.com',
  projectId: 'logapp-21fa6',
  storageBucket: 'logapp-21fa6.appspot.com',
  messagingSenderId: '103507737795',
  appId: '1:103507737795:web:96556a7f1a5096d1fc7676',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export {firebase};
