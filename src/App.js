import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
//import Toast, { BaseToast } from 'react-native-toast-message';
import Navigator from './screens/Navigator';
// import { YellowBox } from 'react-native';

import {Provider} from 'react-redux';
import {store} from './redux/store';

// YellowBox.ignoreWarnings(['Setting a timer']);
console.disableYellowBox = true;

const App = () => {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <Navigator />
      </Provider>
    </SafeAreaProvider>
  );
};

export default App;
